import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-stories',
  templateUrl: './stories.component.html'
})
export class StoriesComponent implements OnInit {

  activeTab = 'tab1';
  @Input() firstTabStories = [];
  @Input() secondTabStories = [];
  @Input() thirdTabStories = [];

  constructor() {
  }

  ngOnInit(): void {
    this.firstTabStories = this.initStoriesContent(8);
    this.secondTabStories = this.initStoriesContent(5);
    this.thirdTabStories = this.initStoriesContent(2);
  }

  toggleAccordion(event: any): void {
    event.target.parentNode.classList.toggle('collapsed');
  }

  private initStoriesContent(numberOfStories: number): Array<any> {
    const stories = [];
    const story = {
      title: 'Accordion header',
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porta imperdiet sem, eget eleifend libero convallis quis. ' +
        'Proin ornare odio at erat tristique, id porttitor tortor semper. Nulla posuere aliquam aliquam. Curabitur maximus sem nec ' +
        'hendrerit sollicitudin.'
    };
    for (let i = 0; i < numberOfStories; i++) {
      stories.push(story);
    }
    return stories;
  }

}
