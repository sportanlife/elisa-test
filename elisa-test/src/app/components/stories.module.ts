import {NgModule} from '@angular/core';
import {StoriesComponent} from './stories.component';

@NgModule({
  imports: [],
  declarations: [StoriesComponent],
  exports: [StoriesComponent]
})

export class StoriesModule {

}
